import store from "./Redux/redux-store";
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {BrowserRouter} from "react-router-dom";
// import StoreContext from "./storeContext";
import {Provider} from "react-redux"


// let renderEntireTree = (state) => {
    ReactDOM.render
    (<React.StrictMode>
            <BrowserRouter>
                <Provider store={store}>
                    <App
                        // state={state}
                        //  store={store}
                        // dispatch={store.dispatch.bind(store)}
                        // addMessage={store.addMessage.bind(store)}
                        // updateNewMessageText={store.updateNewMessageText.bind(store)}
                    />
                </Provider>
            </BrowserRouter>
        </React.StrictMode>,
        document.getElementById('root')
    );
// }
// renderEntireTree(store.getState())

// store.subscribe(() => {
//     // let state = store.getState();
//     renderEntireTree();
// });
