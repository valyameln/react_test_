import React from "react";
import classes from "./Setting.module.css"


const Setting = () => {
    return (
        <div>
            <img className={classes.img}
                src='https://previews.123rf.com/images/oliviart/oliviart2004/oliviart200400873/148386787-setting-icon-isolated-on-white-background-setting-vector-icon-cog-settings-icon-symbol-gear.jpg'
                alt="setting"
            />
        </div>
    );
}
export default Setting;