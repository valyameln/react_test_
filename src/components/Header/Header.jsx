import React from "react";
import classes from "./Header.module.css";
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <header className={classes.header}>
            <img
                src='https://1757140519.rsc.cdn77.org/blog/wp-content/uploads/sites/2/2020/06/h_aster-aries-logo_17.png'
                alt="logo"/>

            <div className={classes.loginBlock}>
                {props.isAuth ? props.login
                : <NavLink to={'/login'}>Login</NavLink>}

            </div>
        </header>
    );
}

export default Header;