import React from "react";
import classes from "./MyPosts.module.css"
import Post from "./Post/Post";


const MyPosts = (props) => {
    // let postData = [
    //     {id: 1, message: 'Hi, how are you?', likesCount: 10},
    //     {id: 2, message: 'Привіт Валентина', likesCount: 34},
    //     {id: 3, message: 'Привіт Антоніна', likesCount: 26},
    // ]

    let postElement =
        props.postData.map(p => <Post message={p.message} key={p.id} like={p.likesCount}/>);

    let newPostElement = React.createRef();

    let onAddPost = () => {
        props.addPost();
        // props.dispatch(addPostActionCreator())
    };

    let onPostChange = () => {
        let text = newPostElement.current.value;
        props.updateNewPostText(text)
        // let action = updateNewPostTextActionCreator(text)
        // props.dispatch(action)
    }

    return (
        <div className={classes.postsBlock}>
            <h2>My posts</h2>
            <div>
                <div>
                    <textarea onChange={onPostChange} ref={newPostElement} value={props.newPostText}/>
                </div>
                <div>
                    <button onClick={onAddPost}>Add post</button>
                </div>
            </div>
            <div className={classes.posts}>
                {postElement}
            </div>
        </div>
    );
}
export default MyPosts;