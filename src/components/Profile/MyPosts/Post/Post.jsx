import React from "react";
import classes from "./Post.module.css"

const Post = (props) => {

    return (
        <div className={classes.item}>
            <img src="https://avatarko.ru/img/avatar/19/paren_i_devushka_18407.jpg" alt="avatar"/>
            {props.message}
            <div>
                <span> like</span>{props.like}
            </div>
        </div>
    );
}
export default Post;