import React from "react";
import {addPostActionCreator, updateNewPostTextActionCreator} from "../../../Redux/profilePageReducer";
import MyPosts from "./MyPosts";
import {connect} from "react-redux";



// const MyPostsContainer = () => {
//
//     // let state = props.store.getState();
//     //
//     // let addPost = () => {
//     //     // props.addPost(text)
//     //     props.store.dispatch(addPostActionCreator())
//     // };
//     //
//     // let onPostChange = (text) => {
//     //     let action = updateNewPostTextActionCreator(text)
//     //     props.store.dispatch(action)
//     //     // let text = newPostElement.current.value;
//     //     // props.updateNewPostText(text)
//     // }
//
//     return (
//         <StoreContext.Consumer>
//             {(store) => {
//                 let state = store.getState();
//
//                 let addPost = () => {
//                     store.dispatch(addPostActionCreator())
//                 };
//
//                 let onPostChange = (text) => {
//                     let action = updateNewPostTextActionCreator(text)
//                     store.dispatch(action)
//                 }
//                 return (
//                     <MyPosts updateNewPostText={onPostChange}
//                              addPost={addPost}
//                              postData={state.profilePage.postData}
//                              newPostText={state.profilePage.newPostText}/>)
//             }
//             }
//         </StoreContext.Consumer>
//     );
// }

let mapStateToProps = (state) => {
    return {
        postData: state.profilePage.postData,
        newPostText: state.profilePage.newPostText  //Дає нам можливість занулти значення
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        updateNewPostText: (text) => {
            let action = updateNewPostTextActionCreator(text)
            dispatch(action);
        },
        addPost: () => {
            dispatch(addPostActionCreator());
        }
    }
}
const MyPostsContainer = connect(mapStateToProps, mapDispatchToProps)(MyPosts);

export default MyPostsContainer;