import React from "react";
import classes from "./ProfileInfo.module.css"
import Preloader from "../../common/Preloader/Preloader";
import sadSmile from "../../../assets/images/sad_smile.png";


const ProfileInfo = (props) => {
    if (!props.profile) {
        return <Preloader/>
    }
    return (
        <div>
            <div>
                <img className={classes.img}
                     src='https://kamendvor.nethouse.ua/static/img/0000/0006/4799/64799954.p39z02buiq.W665.jpg'
                     alt="enlarged_photo"/>
            </div>
            <div className={classes.descriptionBlock}>
                <h2> {props.profile.aboutMe}</h2>
                <img src={props.profile.photos.large}/>
                <p> {props.profile.lookingForAJobDescription}</p>
                ava + description
            </div>
        </div>
    );
}
export default ProfileInfo;