import React from "react";
import classes from "./Users.module.css";
import usersPhoto from "../../assets/images/user.png";
import {NavLink} from "react-router-dom";
import * as axios from "axios";
import {toggleFollowingInProgress} from "../../Redux/usersReducer";

let Users = (props) => {
    let pagesCount = Math.ceil(props.totalUsersCount / props.pageSize);

    let pages = [];
    for (let i = 1; i <= pagesCount; i++) {
        pages.push(i);
    }
    return <div className={classes.wrapperUsers}>
        <div>
            {pages.map(p => {
                return <span className={props.currentPage === p ? classes.pageActive : classes.page}
                             onClick={(e) => {
                                 props.onPageChanged(p);
                             }}>{p}</span>
            })}

        </div>

        {
            // тут вказуємо users, тому що mapStateToProps ми виртаємо саме users
            props.users.map(u => <div key={u.id}>
               <span>
                   <div>
                       <NavLink to={'/profile/' + u.id}>
                           <img src={u.photos.small != null ? u.photos.small : usersPhoto}
                                className={classes.userPhoto}/>
                       </NavLink>
                       </div>
                   <div>
                       {u.followed
                           ? <button disabled={props.followingInProgress.some(id=>id === u.id)} onClick={() => {
                               // debugger;
                               props.toggleFollowingInProgress(true, u.id);
                               axios.delete(`https://social-network.samuraijs.com/api/1.0/follow/${u.id}`, {
                                   withCredentials: true,
                                   headers: {
                                       "API-KEY": "60352b84-6d1a-4425-8bd6-ca0ceca8f5a6"
                                   }
                               })
                                   .then(response => {
                                       if (response.data.resultCode === 0) {
                                           props.unfollow(u.id)
                                       }
                                       props.toggleFollowingInProgress(false, u.id);
                                   });


                           }}>UnFollow</button>
                           : <button disabled={props.followingInProgress.some(id=>id === u.id)} onClick={() => {
                               // debugger
                               props.toggleFollowingInProgress(true, u.id);
                               axios.post(`https://social-network.samuraijs.com/api/1.0/follow/${u.id}`, {}, {
                                   withCredentials: true,
                                   headers: {
                                       "API-KEY": "60352b84-6d1a-4425-8bd6-ca0ceca8f5a6"
                                   }
                               })
                                   .then(response => {
                                       if (response.data.resultCode === 0) {
                                           props.follow(u.id);
                                       }
                                       props.toggleFollowingInProgress(false, u.id);
                                   });
                           }
                           }>Follow</button>
                       }
                   </div>
               </span>
                <span>
                    <span>
                        <div>{u.name}</div><div>{u.status}</div>
                    </span>
                    <span>
                        <div>{"u.location.country"}</div>
                        <div>{"u.location.city"}</div>
                    </span>
                </span>
            </div>)
        }
    </div>

}

export default Users;