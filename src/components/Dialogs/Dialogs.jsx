import React from "react";
import classes from "./Dialogs.module.css"
import DialogItem from "./DialogsItem/DialogsItem";
import Message from "./Message/Message";


const Dialogs = (props) => {
    let state = props.dialogsPage;
    // let dialogs = [
    //     {id: 1, name: 'Valentyna'},
    //     {id: 2, name: 'Antonina'},
    //     {id: 3, name: 'Anatoliy'},
    //     {id: 4, name: 'Volodymyr'},
    //     {id: 5, name: 'Damian'},
    // ];
    // let messages
    //     = [
    //     {id: 1, message: 'Hello)))'},
    //     {id: 2, message: 'How are you?'},
    //     {id: 3, message: 'Hello)))'},
    //     {id: 4, message: '12345678'},
    //     {id: 5, message: 'Are you studying JSC Reaction?'},
    // ];
    let dialogElement =
        state.dialogs.map((d) => <DialogItem img={d.img} key={d.id} name={d.name} id={d.id}/>);

    let messageElement =
       state.messages.map((m) => <Message message={m.message} key={m.id}/>);

    let newMessageBody = state.newMessageText;

    // let newMessageElement = React.createRef();

    let onSendMessageClick = () => {
        props.sendMessage();
    }

    let onNewMessageChange = (event) => {
        let body = event.target.value;
        props.updateNewMessageBody(body)
    }
    return (
        <div className={classes.dialogs}>
            <div className={classes.dialogsItems}>
                {dialogElement}
            </div>
            <div>
                <div className={classes.messages}>
                    <div>{messageElement}</div>
                    <div>
                        <div><textarea placeholder="Enter your message"
                                       onChange={onNewMessageChange}
                                       // ref={newMessageElement}
                                       value={newMessageBody}/>
                        </div>
                        <div>
                            <button onClick={onSendMessageClick}>Send</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
}
export default Dialogs;