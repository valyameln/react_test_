import React from "react";
import {addMessageActionCreator, updateNewMessageTextActionCreator} from "../../Redux/dialogsPageReducer";
import Dialogs from "./Dialogs";
import {connect} from "react-redux";


// const DialogsContainer = () => {
//
//     return (
//         <StoreContext.Consumer>
//             {(store) => {
//                 let state = store.getState().dialogsPage;
//
//                 let onSendMessageClick = () => {
//                     store.dispatch(addMessageActionCreator());
//                 }
//
//                 let onNewMessageChange = (body) => {
//                     store.dispatch(updateNewMessageTextActionCreator(body)); //Відображає те що ми друкуємо
//                 }
//                 return (
//                     <Dialogs
//                         updateNewMessageBody={onNewMessageChange}
//                         sendMessage={onSendMessageClick}
//                         dialogsPage={state}
//                     />
//                 )
//             }
//             }
//         </StoreContext.Consumer>
//
//
//     );
// }

let mapStateToProps = (state) => {
    return {
        dialogsPage: state.dialogsPage,
    }
};
let mapDispatchToProps = (dispatch) =>{
    return{
        updateNewMessageBody: (body)=>{
           dispatch(updateNewMessageTextActionCreator(body));
        },
        sendMessage: ()=>{
           dispatch(addMessageActionCreator());
        }
    }
}

const DialogsContainer = connect(mapStateToProps,mapDispatchToProps)(Dialogs);

export default DialogsContainer;