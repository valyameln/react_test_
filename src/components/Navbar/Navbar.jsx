import React from "react";
import classes from "./Navbar.module.css";
import {NavLink, Route, Routes} from "react-router-dom";
import Friends from "./Friends/Friends";
import Friend from "./Friends/Friend/Friend";
import FriendsContainer from "./Friends/FriendsContainer";

const Navbar = (props) => {

    return (

            <nav className={classes.nav}>
                <div>
                    <NavLink to='/profile'
                             className={navData => navData.isActive ? classes.active : classes.item}>Profile</NavLink>
                </div>
                <div>
                    <NavLink to='/dialogs'
                             className={navData => navData.isActive ? classes.active : classes.item}>Messages</NavLink>
                </div>
                <div className={classes.item}>
                    <NavLink to='/users'
                             className={navData => (navData.isActive ? classes.active : classes.item)}>Users</NavLink>
                </div>
                <div className={classes.item}>
                    <NavLink to='/news'
                             className={navData => (navData.isActive ? classes.active : classes.item)}>News</NavLink>
                </div>
                <div className={classes.item}>
                    <NavLink to='/music'
                             className={navData => (navData.isActive ? classes.active : classes.item)}>Music</NavLink>
                </div>
                <div className={classes.item}>
                    <NavLink to='/setting'
                             className={navData => (navData.isActive ? classes.active : classes.item)}>Setting</NavLink>
                </div>

                <div className={classes.item}>
                    <NavLink to='/friends'
                             className={navData => (navData.isActive ? classes.active : classes.item)}>
                        <h4>Friends</h4>
                    </NavLink>
                    <FriendsContainer
                        // friends={props.sidebar.friends}
                    />
                </div>

            </nav>

    );
}
export default Navbar;