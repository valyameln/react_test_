import React from "react";
import classes from "./Friend.module.css";
import {NavLink} from "react-router-dom";

const Friend = (props) => {

    return (
        <NavLink
            to={'/dialogs/' + props.id}
            className={navData => navData.isActive ? classes.active : classes.friend}>
            <img className={classes.photoFriend}
                 src={props.img} alt=""/>
            <div className={classes.nameFriend}>
                {props.name}</div>
        </NavLink>)
}
export default Friend;