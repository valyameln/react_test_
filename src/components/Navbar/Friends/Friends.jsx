import React from "react";
import classes from "./Friends.module.css";
import Friend from "./Friend/Friend";


const Friends = (props) => {

    // let friends = [
    //     {id: 1, name: 'Valentyna', img:'https://avatarko.ru/img/avatar/21/devushka_blondinka_20146.jpg'},
    //     {id: 2, name: 'Antonina', img: 'https://avatarko.ru/img/avatar/4/devushka_brunetka_3199.jpg'},
    //     {id: 3, name: 'Anatoliy', img: 'https://avatarko.ru/img/avatar/11/film_muzhchina_10363.jpg'},
    //     {id: 4, name: 'Volodymyr', img: 'https://avatarko.ru/img/avatar/6/siluet_paren_i_devushka_5670.jpg'},
    //     {id: 5, name: 'Damian', img: 'https://avatarko.ru/img/avatar/19/deti_kniga_18215.jpg'},
    // ];
    let state = props.sidebar;

    let friendsElement =
        state.friends.map(f => <Friend id={f.id} key={f.id} name={f.name} img={f.img}/>)

    return (
        <div className={classes.friends}>
            {friendsElement}
        </div>
    );
}
export default Friends;