import React from "react";
import Friends from "./Friends";
import {connect} from "react-redux";




// const FriendsContainer = () => {
//
//     return (
//         <StoreContext.Consumer>
//             {(store)=>{
//                 let state = store.getState().sidebar;
//
//                 return(
//                     <Friends
//                         sidebar={state}
//                     />
//                 )
//             }
//             }
//         </StoreContext.Consumer>
//     );
// }

let mapStateToProps = (state) => {
    return{
        sidebar:state.sidebar
    }
}

let mapDispatchToProps = (dispatch)=>{
    return{

    }
}
const FriendsContainer = connect(mapStateToProps,mapDispatchToProps)(Friends);

export default FriendsContainer;