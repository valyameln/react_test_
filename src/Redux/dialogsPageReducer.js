const ADD_MESSAGE = 'ADD-MESSAGE';
const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE-NEW-MESSAGE-TEXT';


let initialState = {
    dialogs: [
        {id: 1, name: 'Valentyna', img: 'https://avatarko.ru/img/avatar/21/devushka_blondinka_20146.jpg'},
        {id: 2, name: 'Antonina', img: 'https://avatarko.ru/img/avatar/4/devushka_brunetka_3199.jpg'},
        {id: 3, name: 'Anatoliy', img: 'https://avatarko.ru/img/avatar/11/film_muzhchina_10363.jpg'},
        {id: 4, name: 'Volodymyr', img: 'https://avatarko.ru/img/avatar/6/siluet_paren_i_devushka_5670.jpg'},
        {id: 5, name: '111111111Damian', img: 'https://avatarko.ru/img/avatar/19/deti_kniga_18215.jpg'},
    ],
    messages: [
        {id: 1, message: 'Hello)))'},
        {id: 2, message: 'How are you?'},
        {id: 3, message: 'Hello)))'},
        {id: 4, message: '12345678'},
        {id: 5, message: 'Are you studying JSC Reaction?'},
        // {id: 6, message: 'Hello)))'},
        // {id: 7, message: 'How are you?'},
        // {id: 8, message: 'Hello)))'},
        // {id: 9, message: '12345678'},
        // {id: 10, message: 'Are you studying JSC Reaction?'},
    ],
    newMessageText: 'New Message'
};

const dialogsReduce = (state = initialState, action) => {

    switch (action.type) {
        case UPDATE_NEW_MESSAGE_TEXT: {
            return  {
                ...state,
                newMessageText: action.body
            };
            // stateCopy.newMessageText = action.body;    //Ми змінили _state
        }
        case ADD_MESSAGE: {
            let newMessage = state.newMessageText;
            return  {...state,
                newMessageText:'', //Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку надыслати
                messages: [...state.messages, {id: 11, message: newMessage}] //теж саме якби ми зробили push({id: 11, message: newMessage});
            };
            // stateCopy.messages = [...state.messages];
            // stateCopy.newMessageText = ''; //Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку надыслати
            // stateCopy.messages.push({id: 11, message: newMessage});
        }
        default:
            return state;
    }
    // if (action.type === UPDATE_NEW_MESSAGE_TEXT) {
    //     state.newMessageText = action.body;    //Ми змінили _state
    //                            //Ми викликали _callSubscriber і передали змінений вже _state
    // }
    // else if (action.type === ADD_MESSAGE) {
    //     let newMessage = state.newMessageText;
    //     state.newMessageText = ''; //Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку надыслати
    //     state.messages.push({id: 11, message: newMessage});
    // }

}
export const addMessageActionCreator = () => ({type: ADD_MESSAGE});
export const updateNewMessageTextActionCreator = (text) => ({
    type: UPDATE_NEW_MESSAGE_TEXT,
    body: text
})

export default dialogsReduce;
