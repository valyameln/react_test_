import sidebarReduce from "./sidebarReducer";
import profileReduce from "./profilePageReducer";
import dialogsReduce from "./dialogsPageReducer";

// const ADD_POST = 'ADD-POST';
// const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
// const ADD_MESSAGE = 'ADD-MESSAGE';
// const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE-NEW-MESSAGE-TEXT';


let store = {
    _state: {
        profilePage: {
            postData: [
                {id: 1, message: 'Hi, how are you?', likesCount: 10},
                {id: 2, message: 'Привіт Валентина', likesCount: 34},
                {id: 3, message: 'Привіт Антоніна', likesCount: 26},
            ],
            newPostText: 'it-kamasutra'
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: 'Valentyna', img: 'https://avatarko.ru/img/avatar/21/devushka_blondinka_20146.jpg'},
                {id: 2, name: 'Antonina', img: 'https://avatarko.ru/img/avatar/4/devushka_brunetka_3199.jpg'},
                {id: 3, name: 'Anatoliy', img: 'https://avatarko.ru/img/avatar/11/film_muzhchina_10363.jpg'},
                {id: 4, name: 'Volodymyr', img: 'https://avatarko.ru/img/avatar/6/siluet_paren_i_devushka_5670.jpg'},
                {id: 5, name: '111111111Damian', img: 'https://avatarko.ru/img/avatar/19/deti_kniga_18215.jpg'},
            ],
            messages: [
                {id: 1, message: 'Hello)))'},
                {id: 2, message: 'How are you?'},
                {id: 3, message: 'Hello)))'},
                {id: 4, message: '12345678'},
                {id: 5, message: 'Are you studying JSC Reaction?'},
                // {id: 6, message: 'Hello)))'},
                // {id: 7, message: 'How are you?'},
                // {id: 8, message: 'Hello)))'},
                // {id: 9, message: '12345678'},
                // {id: 10, message: 'Are you studying JSC Reaction?'},
            ],
            newMessageText: 'New Message'
        },
        sidebar: {
            friends: [
                {id: 1, name: 'Valentyna', img: 'https://avatarko.ru/img/avatar/21/devushka_blondinka_20146.jpg'},
                {id: 2, name: 'Antonina', img: 'https://avatarko.ru/img/avatar/4/devushka_brunetka_3199.jpg'},
                {id: 3, name: 'Anatoliy', img: 'https://avatarko.ru/img/avatar/11/film_muzhchina_10363.jpg'},
                {id: 4, name: 'Volodymyr', img: 'https://avatarko.ru/img/avatar/6/siluet_paren_i_devushka_5670.jpg'},
                {id: 5, name: 'Damian', img: 'https://avatarko.ru/img/avatar/19/deti_kniga_18215.jpg'},
            ],
        },
    },
    _callSubscriber() {
        console.log("PRUVIT")
    },

    getState() {
        // debugger;
        return this._state
    },
    subscribe(observer) {
        this._callSubscriber = observer         //хтось викликав subscribe і підписався на зміни в UPDATE_NEW_MESSAGE_TEXT
    },

    // addPost() {
    //     // debugger;
    //     let newPost = {
    //         id: 5,
    //         message: this._state.profilePage.newPostText,
    //         likesCount: 5
    //     };
    //     this._state.profilePage.postData.push(newPost);
    //     this._state.profilePage.newPostText = ''; //Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку
    //     this._callSubscriber(this._state);
    // },

    // updateNewPostText(newText) {
    //     this._state.profilePage.newPostText = newText;
    //     this._callSubscriber(this._state);
    // },

    // addMessage() {
    //     // debugger;
    //     let newMessage = {
    //         id: 11,
    //         message: this._state.dialogsPage.newMessageText
    //     };
    //     this._state.dialogsPage.messages.push(newMessage);
    //     this._state.dialogsPage.newMessageText = ''; //Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку надыслати
    //     this._callSubscriber(this._state);
    // },

    // updateNewMessageText(newText) {
    //     this._state.dialogsPage.newMessageText = newText;
    //     this._callSubscriber(this._state);
    // },

    dispatch(action) {
        // debugger;
        this._state.profilePage = profileReduce(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReduce(this._state.dialogsPage, action);
        this._state.sidebar = sidebarReduce(this._state.sidebar, action);

        this._callSubscriber(this._state);

    }
}

// export const addPostActionCreator = () => {
//     return {
//         type: ADD_POST
//     }
// }
// export const updateNewPostTextActionCreator = (text) => {
//     return {
//         type: UPDATE_NEW_POST_TEXT,
//         newText: text
//     }
// }

// export const addMessageActionCreator = () => ({type: ADD_MESSAGE});
// export const updateNewMessageTextActionCreator = (text) => ({
//         type: UPDATE_NEW_MESSAGE_TEXT,
//         body: text
//     })


export default store;
window.store = store

