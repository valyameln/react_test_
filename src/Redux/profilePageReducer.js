const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';
const SET_USER_PROFILE = 'SET-USER-PROFILE'


let initialState = {
    postData: [
        {id: 1, message: 'Hi, how are you?', likesCount: 10},
        {id: 2, message: 'Привіт Валентина', likesCount: 34},
        {id: 3, message: 'Привіт Антоніна', likesCount: 26},
    ],
    newPostText: 'it-kamasutra',
    profile: null
};

const profileReduce = (state = initialState, action) => {
    switch (action.type) {
        case ADD_POST: {
            let newPost = {
                id: 5,
                message: state.newPostText,
                likesCount: 5
            };
            return {
                ...state,
                postData: [...state.postData, newPost],
                newPostText: ''
            };
            // stateCopy.postData = [...state.postData];
            // stateCopy.postData.push(newPost);
            // stateCopy.newPostText = '';//Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку
            // return stateCopy;
        }
        case UPDATE_NEW_POST_TEXT: {
            return {
                ...state,
                newPostText: action.newText
            };
            // stateCopy.newPostText = action.newText;
            // return stateCopy;
        }
        case SET_USER_PROFILE: {
            return {...state, profile: action.profile}
        }
        default:
            return state;
    }

    // if (action.type === ADD_POST) {
    //     let newPost = {
    //         id: 5,
    //         message: state.newPostText,
    //         likesCount: 5
    //     };
    //     state.postData.push(newPost);
    //     state.newPostText = ''; //Зануляємо наше значення textarea після надсилання, а саме після нажатття на кнопку
    // } else if (action.type === UPDATE_NEW_POST_TEXT) {
    //     state.newPostText = action.newText;
    // }
    // return state;
}

export const addPostActionCreator = () => {
    return {
        type: ADD_POST
    }
}
export const setUserProfile = (profile) => ({type: SET_USER_PROFILE, profile})
export const updateNewPostTextActionCreator = (text) => {
    return {
        type: UPDATE_NEW_POST_TEXT,
        newText: text
    }
}
export default profileReduce;