import {combineReducers, createStore} from "redux";
import profileReduce from "./profilePageReducer";
import sidebarReduce from "./sidebarReducer";
import dialogsReduce from "./dialogsPageReducer";
import usersReducer from "./usersReducer";
import authReduce from "./auth-reducer";

let reducers = combineReducers({
    profilePage: profileReduce,
    dialogsPage: dialogsReduce,
    sidebar: sidebarReduce,
    usersPage: usersReducer,
    auth: authReduce
});

let store = createStore(reducers);
window.store = store;

export default store;